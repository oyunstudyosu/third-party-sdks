﻿namespace Scripts.Service
{
    public enum ServiceType
    {
        Default,
        Local,
        GooglePlay,
        Facebook,
        GameCenter,
        Unity,
        Firebase,
        Twitter,
        Instagram
    }
}