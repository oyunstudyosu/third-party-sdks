﻿namespace Scripts.Service.Authentication
{
    public enum AuthenticationEvent
    {
        LoggedIn,
        Logout,
        LoginError,
        Reset,
        Registered,
        Error,
        Linked,
        LoggingIn
    }
}