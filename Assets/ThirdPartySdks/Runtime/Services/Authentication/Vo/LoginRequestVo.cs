﻿using System;
using Scripts.Service.Authentication.Enums;
using UnityEngine;

namespace Scripts.Service.Authentication.Vo
{
    [Serializable]
    public class LoginRequestVo
    {
        public LoginType Type { get; set; }

        public string ExternalId { get; set; }

        public string DeviceId { get; set; }

        public string Version { get; set; }

        public string Platform { get; set; }

        public LoginRequestVo(LoginType type, string externalId, string deviceId,string platform)
        {
            Type = type;
            ExternalId = externalId;
            DeviceId = deviceId;
            Version = Application.version;
            Platform = platform;
        }
    }
}