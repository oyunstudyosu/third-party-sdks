﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Scripts.Service.Authentication.Events;

namespace Scripts.Service.Authentication.Vo
{
  [Serializable]
  public class AuthenticationErrorVo
  {
    [JsonConverter(typeof(StringEnumConverter))]
    public AuthenticationServiceEvent ServiceEventType { get; set; }

    public List<ProviderVo> ModuleState { get; set; }

    public bool Connected { get; set; }

    public bool IsDeviceAccount { get; set; }

    public string UserId { get; set; }

    public string DeviceId { get; set; }
  }
}
