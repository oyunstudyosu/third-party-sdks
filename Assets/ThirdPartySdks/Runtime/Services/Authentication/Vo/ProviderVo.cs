﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Scripts.Service.Authentication.Providers;

namespace Scripts.Service.Authentication.Vo
{
  [Serializable]
  public class ProviderVo
  {
    [JsonConverter(typeof(StringEnumConverter))]
    public ServiceType Type
    {
      get { return Service.Type; }
    }

    [JsonIgnore]
    private IAuthenticationProvider _service;

    [JsonIgnore]
    public IAuthenticationProvider Service
    {
      get { return _service; }
      set { _service = value; }
    }

    public int Priority
    {
      get { return Service.Priority; }
    }

    public bool IsInited { get; set; }

    public bool IsLinked { get; set; }

    public bool Connected
    {
      get { return Service.Connected; }
    }

    public string AccessToken
    {
      get { return Service.AccessToken; }
    }

    public string UserId
    {
      get { return Service.UserId; }
    }

    public string Username
    {
      get { return Service.Username; }
    }

    public string PhotoUrl
    {
      get { return Service.PhotoUrl; }
    }

    public string Email
    {
      get { return Service.Email; }
    }
  }
}