﻿using System;
using Scripts.Service.Authentication.Enums;
using UnityEngine;

namespace Scripts.Service.Authentication.Vo
{
  [Serializable]
  public class LinkRequestVo
  {
    public LoginType LinkType;

    public string AccessToken;

    public string PhotoUrl;

    public string Email;

    public string UserName;

    public string DeviceId;

    public LinkRequestVo(LoginType type, string token, string photoUrl,string email,string userName)
    {
      LinkType = type;
      AccessToken = token;
      PhotoUrl = photoUrl;
      Email = email;
      DeviceId = SystemInfo.deviceUniqueIdentifier;
      UserName = userName;
    }
  }
}