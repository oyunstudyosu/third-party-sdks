﻿using System;

namespace Scripts.Service.Authentication.Vo
{
  [Serializable]
  public class LoginResponseVo
  {
    public bool Update;

    public bool IsDeviceAccount;

    public string Token;

    public int UserId;
  }
}