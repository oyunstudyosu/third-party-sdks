﻿using System;

namespace Scripts.Service.Authentication.Vo
{
    [Serializable]
    public class GameCenterRequestVo
    {
      public string PublicKeyUrl;

      public string Timestamp;

      public string Signature;

      public string Salt;

      public string PlayerId;

      public string Alias;

      public string BundleId;

      public GameCenterRequestVo(string url, string timestamp, string signature, string salt, string playerId, string alias, string bundleId)
      {
        PublicKeyUrl = url;
        Timestamp = timestamp;
        Signature = signature;
        Salt = salt;
        PlayerId = playerId;
        Alias = alias;
        BundleId = bundleId;
      }
    }
}