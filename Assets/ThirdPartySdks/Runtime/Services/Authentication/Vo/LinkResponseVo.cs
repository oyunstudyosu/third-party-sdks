﻿using Scripts.Service.Authentication.Enums;

namespace Scripts.Service.Authentication.Vo
{
    public class LinkResponseVo
    {
        public int ResponseCode;

        public LoginType LinkType;

        public bool Shared;
    }
}