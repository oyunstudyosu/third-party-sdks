﻿namespace Scripts.Service.Authentication.Events
{
    public enum AuthenticationServiceEvent
    {
        Registered,
        RegisterError,
        LoggedIn,
        LoginError,
        Linked,
        LinkError,
        Reset,
        ResetError,
        AlreadyLinked,
        SwitchUser,
        ProviderRegister,
        LoggedOut,
        AlreadyLoggedIn
    }
}