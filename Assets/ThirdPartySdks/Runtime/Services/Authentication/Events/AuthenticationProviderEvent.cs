﻿namespace Scripts.Service.Authentication.Events
{
    public enum AuthenticationProviderEvent
    {
        LoggedIn,
        Logout,
        LoginError,
        Inited,
        PhotoLoaded,
        Cancelled
    }
}