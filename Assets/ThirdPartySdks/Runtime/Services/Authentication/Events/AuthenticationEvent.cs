﻿namespace Scripts.Service.Authentication.Events
{
  public enum AuthenticationEvent
  {
    Registered,
    LoggedIn,
    Linked,
    Reset,
    Error,
    Logout,
    LoggingIn
  }
}