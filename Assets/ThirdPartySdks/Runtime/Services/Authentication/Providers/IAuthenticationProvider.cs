﻿namespace Scripts.Service.Authentication.Providers
{
  public interface IAuthenticationProvider
  {
    void Init();

    void Login();

    bool Connected { get; }

    string Username { get; }

    string UserId { get; }

    string AccessToken { get; }

    void Logout();

    ServiceType Type { get; }

    int Priority { get; }

    string PhotoUrl { get; }

    string Email { get; }
  }
}