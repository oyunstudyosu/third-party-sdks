﻿#if UNITY_ANDROID && UNITY_ADS
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using strange.extensions.context.api;
using strange.extensions.dispatcher.eventdispatcher.api;
using Service.Authentication.Events;
using UnityEngine;

namespace Service.Authentication.Providers.Impl
{
    public class GooglePlayAuthService : IAuthenticationProvider
    {
        [Inject(ContextKeys.CONTEXT_DISPATCHER)]
        public IEventDispatcher dispatcher { get; set; }

        public IEventDispatcher Dispatcher
        {
            get { return dispatcher; }
            set { dispatcher = value; }
        }

        private string _username;

        private string _userId;

        private bool _connected;

        private PlayGamesPlatform _service;

        private bool _debugMode;

        private void Log(string message)
        {
            _debugMode = false;
            if (_debugMode) Debug.Log(message);
        }

        public void Init()
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
//        .AddOauthScope("PROFILE")
//        .AddOauthScope("EMAIL").AddOauthScope("GAMES")
//        .RequestServerAuthCode(false)
                .Build();
            PlayGamesPlatform.DebugLogEnabled = true;
            PlayGamesPlatform.InitializeInstance(config);

            _service = PlayGamesPlatform.Activate();

            if (!PlayerPrefs.HasKey(GetType().FullName))
            {
                dispatcher.Dispatch(AuthenticationProviderEvent.Inited, ServiceType.GooglePlay);
                return;
            }

            Login();
        }

        public void Login()
        {
            Log("GooglePlayAuthService.Login");

            if (Connected)
                return;

            _service.Authenticate(OnAuthenticate);
        }

        private void OnAuthenticate(bool result)
        {
            Log("GooglePlayAuthService.OnAuthenticate");
            if (result)
            {
                _connected = true;
                _username = _service.localUser.userName;
                _userId = _service.localUser.id;
                PlayerPrefs.SetInt(GetType().FullName, 1);
                dispatcher.Dispatch(AuthenticationProviderEvent.LoggedIn, ServiceType.GooglePlay);
            }
            else
            {
                dispatcher.Dispatch(AuthenticationProviderEvent.LoginError, ServiceType.GooglePlay);
            }
        }

        public bool Connected
        {
            get { return _connected; }
        }

        public string Username
        {
            get { return _username; }
        }

        public string UserId
        {
            get { return _userId; }
        }

        public void Logout()
        {
            if (!Connected)
                return;

            _connected = false;
            _service.SignOut();
            _username = "None";
            _userId = string.Empty;
            PlayerPrefs.DeleteKey(GetType().FullName);
            dispatcher.Dispatch(AuthenticationProviderEvent.Logout, ServiceType.GooglePlay);
        }

        public string AccessToken
        {
            get
            {
                //TODO: access token alınacak
                return _userId;
            }
        }

        public ServiceType Type
        {
            get { return ServiceType.GooglePlay; }
        }

        public int Priority
        {
            get { return 2; }
        }

        public string PhotoUrl
        {
            get
            {
                if (!_connected)
                    return string.Empty;
                return _service.GetUserImageUrl();
            }
        }

        public string Email
        {
            get { return string.Empty; }
        }
    }
}
#endif