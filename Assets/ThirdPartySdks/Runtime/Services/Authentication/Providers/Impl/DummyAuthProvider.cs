﻿using System.Collections.Generic;
using strange.extensions.context.api;
using strange.extensions.dispatcher.eventdispatcher.api;
using Scripts.Service.Authentication.Events;
using UnityEngine;

namespace Scripts.Service.Authentication.Providers.Impl
{
  public class DummyAuthProvider : IAuthenticationProvider
  {
    [Inject(ContextKeys.CONTEXT_DISPATCHER)]
    public IEventDispatcher dispatcher { get; set; }

    public bool _connected;

    public string _username;

    public string _email;

    public string _userId;

    public bool Error;

    public void Init()
    {
      Priority = 2;

      if (_connected)
        return;

      dispatcher.Dispatch(AuthenticationProviderEvent.Inited, Type);
    }

    public void Login()
    {
      if (Connected)
        return;

      if (Error)
      {
        dispatcher.Dispatch(AuthenticationProviderEvent.LoginError, Type);
        return;
      }

      _connected = true;
      _username = GetRandom(new[] {"John", "Sally", "Jimmy", "Caroline"});
      dispatcher.Dispatch(AuthenticationProviderEvent.LoggedIn, Type);
    }

    public bool Connected
    {
      get { return _connected; }
    }

    public string Username
    {
      get { return _username; }
    }

    public string UserId
    {
      get { return _userId; }
    }

    public Texture2D Photo { get; set; }

    public void Logout()
    {
      if (!Connected)
        return;
      _connected = false;
      _username = string.Empty;
      dispatcher.Dispatch(AuthenticationProviderEvent.Logout, Type);
    }

    private static string GetRandom(IList<string> list)
    {
      return list[Random.Range(0, list.Count)];
    }

    public string AccessToken { get; private set; }
    public string PhotoUrl { get; set; }
    public string Email {
      get { return _email; }
    }

    public ServiceType Type
    {
      get { return ServiceType.Facebook; }
    }

    public int Priority { get; set; }
  }
}