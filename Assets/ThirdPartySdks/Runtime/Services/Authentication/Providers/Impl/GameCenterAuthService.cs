﻿#if UNITY_IOS

using AOT;
using Firebase;
using Newtonsoft.Json;
using Online;
using strange.extensions.context.api;
using strange.extensions.dispatcher.eventdispatcher.api;
using Scripts.Service;
using Scripts.Service.Authentication.Events;
using Scripts.Service.Authentication.Providers;
using Scripts.Service.Authentication.Vo;
using UnityEngine;

namespace Assets.Scripts.Service.Authentication.Providers.Impl
{
  public class GameCenterAuthService : IAuthenticationProvider
  {
    [JsonIgnore]
    [Inject(ContextKeys.CONTEXT_DISPATCHER)]
    public IEventDispatcher dispatcher { get; set; }

    [JsonIgnore]
    public IEventDispatcher Dispatcher
    {
      get { return dispatcher; }
      set { dispatcher = value; }
    }

    private string _username;

    private string _userId;

    private string _email;

    private bool _connected;

    private bool _debugMode;

    private string _token;

    private static GameCenterAuthService Instance;

    private void Log(string message)
    {
      _debugMode = false;
      if (_debugMode) Debug.Log(message);
    }

    public void Init()
    {
      Instance = this;

      Debug.Log(this.GetType().FullName);
      Debug.Log(PlayerPrefs.HasKey(this.GetType().FullName));
      if (!PlayerPrefs.HasKey(this.GetType().FullName))
      {
        Debug.Log("Inited Game center");
        dispatcher.Dispatch(AuthenticationProviderEvent.Inited,ServiceType.GameCenter);
        return;
      }

      Login();
    }

    public void Login()
    {
      Debug.Log("GameCenterPlayAuthService.Login");

      if (Connected)
        return;

      if (!Social.localUser.authenticated)
        Social.localUser.Authenticate(OnAuthenticate);
      else
        LoginSuccess();
    }

    private void LoginSuccess()
    {
      Debug.Log("OnAuthenticate " + "userId" + _userId + "username" + _username);

      GameCenterSignature.Generate(OnSucceeded, OnFailed);

    }

    private void SetUser(string token)
    {
      _token = token;
      _userId = Social.localUser.id;
      _username = Social.localUser.userName;
      _connected = true;
      PlayerPrefs.SetInt(this.GetType().FullName,1);
      dispatcher.Dispatch(AuthenticationProviderEvent.LoggedIn, ServiceType.GameCenter);
    }

    private void OnAuthenticate(bool result)
    {
      Debug.Log("GameCenterPlayAuthService.OnAuthenticate");
      if (result)
      {
        LoginSuccess();
      }
      else
      {
        Debug.LogError("Login Error!");
        dispatcher.Dispatch(AuthenticationProviderEvent.LoginError, ServiceType.GameCenter);
      }
    }

    public bool Connected
    {
      get
      {
        return _connected;
      }
    }

    public string Username
    {
      get { return _username; }
    }

    public string UserId
    {
      get { return _userId; }
    }

    public Texture2D Photo { get; set; }

    public void Logout()
    {
      _connected = false;
      _username = string.Empty;
      _userId = string.Empty;
      _token = string.Empty;
      PlayerPrefs.DeleteKey(this.GetType().FullName);
      dispatcher.Dispatch(AuthenticationProviderEvent.Logout, ServiceType.GameCenter);
    }

    public string AccessToken
    {
      get { return _token; }
    }

    public string PhotoUrl { get; set; }
    public string Email {
      get { return _email; }
    }

    [MonoPInvokeCallback(typeof(GameCenterSignature.OnSucceeded))]
    private static void OnSucceeded(string publicKeyUrl, ulong timestamp, string signature, string salt, string playerId,
      string alias, string bundleId)
    {
      Debug.Log("GameCenterAuthService->OnSucceeded");

      GameCenterRequestVo gameCenterRequest = new GameCenterRequestVo(publicKeyUrl, timestamp.ToString(), signature,
        salt, playerId, alias, bundleId);

      string token = JsonConvert.SerializeObject(gameCenterRequest);

      Debug.Log("token " + token);

      Instance.SetUser(token);
    }

    [MonoPInvokeCallback(typeof(GameCenterSignature.OnFailed))]
    private static void OnFailed(string reason)
    {
      Debug.Log("GameCenterAuthService->OnFailed");
      Debug.Log("Failed to authenticate with gamecenter: " + reason);
      Instance.Logout();
    }

    public ServiceType Type
    {
      get { return ServiceType.GameCenter; }
    }

    public int Priority
    {
      get { return 2; }
    }
  }
}
#endif