﻿namespace Scripts.Service.Authentication.Enums
{
    public enum LoginType
    {
        DEVICE,
        GAMECENTER,
        FACEBOOK,
        GOOGLE
    }
}