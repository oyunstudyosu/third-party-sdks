﻿using UnityEngine;

namespace Scripts.Service.Tracking.Imp
{
    public class DummyTrackingService : ITrackingService
    {
        public void Earn(string source, string currency, float price)
        {
        }

        public void Purchase(string item, string currency, float price)
        {
        }

        public void Transaction(string product, string currency, float price, string receipt, string signature)
        {
        }

        public void Screen(string name)
        {
        }

        public void Event(string category, string action, string label, int value)
        {
        }

        public void Event(string category, string action, string label)
        {
        }

        public void Event(string category, string action)
        {
        }

        public void Error(string name)
        {
        }

        public void Event(string category, string action, string label, string value)
        {
        }

        public void Heatmap(string category, Vector3 position)
        {
        }
    }
}