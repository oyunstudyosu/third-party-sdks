﻿using UnityEngine;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DigitalOpus.MB.Core
{
  public class MB3_CopyBoneWeights
  {
    private List<int> _seamVerts = new List<int>();
    private Vector3[] _srcVerts;
    private BoneWeight[] _srcBw;
    private Vector3[] _srcNormal;
    private Vector4[] _srcTangent;
    private Vector2[] _srcUVs;
    private bool _failed;
    private Vector3[] _otherVerts;
    private BoneWeight[] _otherBWs;
    private Vector4[] _otherTangents;
    private Vector3[] _otherNormals;
    private int _index0;
    private int _numMatches;

    public void CopyBoneWeightsFromSeamMeshToOtherMeshes(float radius, Mesh seamMesh, Mesh[] targetMeshes)
    {
      //Todo make sure meshes are assets
      _seamVerts.Clear();
      if (seamMesh == null)
      {
        Debug.LogError(string.Format("The SeamMesh cannot be null"));
        return;
      }

      if (seamMesh.vertexCount == 0)
      {
        Debug.LogError(
          "The seam mesh has no vertices. Check that the Asset Importer for the seam mesh does not have 'Optimize Mesh' checked.");
        return;
      }

      _srcVerts = seamMesh.vertices;
      _srcBw = seamMesh.boneWeights;
      _srcNormal = seamMesh.normals;
      _srcTangent = seamMesh.tangents;
      _srcUVs = seamMesh.uv;

      if (_srcUVs.Length != _srcVerts.Length)
      {
        Debug.LogError(
          "The seam mesh needs uvs to identify which vertices are part of the seam. Vertices with UV > .5 are part of the seam. Vertices with UV < .5 are not part of the seam.");
        return;
      }

      for (int i = 0; i < _srcUVs.Length; i++)
      {
        if (_srcUVs[i].x > .5f && _srcUVs[i].y > .5f)
        {
          _seamVerts.Add(i);
        }
      }

      if (_seamVerts.Count == 0)
      {
        Debug.LogError(
          "None of the vertices in the Seam Mesh were marked as seam vertices. To mark a vertex as a seam vertex the UV" +
          " must be greater than (.5,.5). Vertices with UV less than (.5,.5) are excluded.");
        return;
      }

      //validate
      _failed = false;
      for (var meshIdx = 0; meshIdx < targetMeshes.Length; meshIdx++)
      {
        if (targetMeshes[meshIdx] == null)
        {
          Debug.LogError(string.Format("Mesh {0} was null", meshIdx));
          _failed = true;
        }

        if (radius < 0f)
        {
          Debug.LogError("radius must be zero or positive.");
        }
      }

      if (_failed)
      {
        return;
      }

      Debug.Log("CopyBoneStart");

      foreach (var tm in targetMeshes)
      {
        Debug.Log(" ------> TM Start");

        _otherVerts = tm.vertices;
        _otherBWs = tm.boneWeights;
        _otherNormals = tm.normals;
        _otherTangents = tm.tangents;

        _numMatches = 0;
        for (var i = 0; i < _otherVerts.Length; i++)
        {
          foreach (var seemVert in _seamVerts)
          {
            if (Vector3.Distance(_otherVerts[i], _srcVerts[seemVert]) <= radius)
            {
              Debug.Log(i + " - " + seemVert);
              _numMatches++;
              _otherBWs[i] = _srcBw[seemVert];
              _otherVerts[i] = _srcVerts[seemVert];
              if (_otherNormals.Length == _otherVerts.Length && _srcNormal.Length == _srcNormal.Length)
              {
                _otherNormals[i] = _srcNormal[seemVert];
              }

              if (_otherTangents.Length == _otherVerts.Length && _srcTangent.Length == _srcVerts.Length)
              {
                _otherTangents[i] = _srcTangent[seemVert];
              }
            }
          }
        }

        if (_numMatches > 0)
        {
          Debug.Log(JsonConvert.SerializeObject(_otherBWs));
          tm.vertices = _otherVerts;
          tm.boneWeights = _otherBWs;
          tm.normals = _otherNormals;
          tm.tangents = _otherTangents;
        }

        Debug.Log(" ------> TM End > " + _numMatches);
      }
    }
  }
}